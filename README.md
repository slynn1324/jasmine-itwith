# jasmine-itwith

## Inspired by:

- https://github.com/dfkaye/where.js
- http://blog.jphpsf.com/2012/08/30/drying-up-your-javascript-jasmine-tests

## Installation

    npm install jasmine-itwith

or just download jasmine-itwith.js.


Just make sure to include jasmine-itwith.js before your tests.

## karma.conf.js

    files: [ 
      ...
      'node_modules/jasmine-itwith/karma-itwith.js',
      ...
    ],
