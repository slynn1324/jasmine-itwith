/**
 * Wrap the Jasmine 'it' function wih a delegate
 * that accepts an additional 'data' parameter.
 *
 * supports indexed replacement tokens in the spec name. 
 * e.g., {0} plus {1} should be {2}
 */
function itWith(name, func, data){
	var _this = this;
	data.forEach(function(datum){
		
		var datumAsArr = datum

		// wrap a singular item as an array so it can be passed to apply
		if ( Object.prototype.toString.call(datumAsArr) !== '[object Array]'){
            datumAsArr = [datumAsArr];
        }

		var args = [name];
		Array.prototype.push.apply(args, datumAsArr);

		var itName = strFormat.apply(_this, args);

		it(itName, function(){
			func.apply(_this, datumAsArr);
		});
	});

	// replace indexed {0} tokens
	function strFormat(format) {
	    var args = Array.prototype.slice.call(arguments, 1);
	    return format.replace(/{(\d+)}/g, function(match, number) { 
	    	if ( typeof args[number] === 'undefined' ){
	    		return match;
	    	} else if ( typeof args[number] === 'object' ){
	    		return JSON.stringify(args[number]);
	    	} else {
	    		return args[number];
	    	}
	    });
	}
}